frappe.listview_settings['Booking Details'] = {
	onload: function(listview) {
		
		listview.page.add_menu_item(__("Per booking report"), function() {
			frappe.set_route("per-booking-report");
		});
		listview.page.add_menu_item(__("VIP suite booking summary"), function() {
			frappe.set_route("vip-suite-booking");
		});
		// listview.page.add_menu_item(__("Summary per authority"), function() {
		// });
		listview.page.add_menu_item(__("Vehicle Detailed Graph"), function() {
			frappe.set_route("vehicle-detailed-gra");
		});
		

	}
};