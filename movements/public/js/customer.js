frappe.ui.form.on('Customer', {
	first_name: function(frm) {
		frm.doc.customer_name = "";
		if(frm.doc.first_name){
			frm.doc.customer_name +=frm.doc.first_name; 
		}
		if(frm.doc.middle_name){
			frm.doc.customer_name +=" "+frm.doc.middle_name;
		}
		if(frm.doc.last_name){
			frm.doc.customer_name +=" "+frm.doc.last_name;
		}
        return frm.doc.customer_name;
    },
    middle_name: function(frm) {
        frm.doc.customer_name = "";
		if(frm.doc.first_name){
			frm.doc.customer_name +=frm.doc.first_name; 
		}
		if(frm.doc.middle_name){
			frm.doc.customer_name +=" "+frm.doc.middle_name;
		}
		if(frm.doc.last_name){
			frm.doc.customer_name +=" "+frm.doc.last_name;
		}
        return frm.doc.customer_name;
    },
    last_name: function(frm) {
        frm.doc.customer_name = "";
		if(frm.doc.first_name){
			frm.doc.customer_name +=frm.doc.first_name; 
		}
		if(frm.doc.middle_name){
			frm.doc.customer_name +=" "+frm.doc.middle_name;
		}
		if(frm.doc.last_name){
			frm.doc.customer_name +=" "+frm.doc.last_name;
		}
        return frm.doc.customer_name;
    }
});